/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.1  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */
#include "nfapi/open-nFAPI/nfapi/public_inc/nfapi_interface.h"

#if !defined(NFAPI_VNF_H__)
#define NFAPI_VNF_H__

void configure_nfapi_vnf(char *vnf_addr, int vnf_p5_port);
void configure_nfapi_vnf_nb_iot(char *vnf_addr, int vnf_p5_port);
int oai_nfapi_dl_config_req(nfapi_dl_config_request_t *dl_config_req);
int oai_nfapi_tx_req(nfapi_tx_request_t *tx_req);
int oai_nfapi_hi_dci0_req(nfapi_hi_dci0_request_t *hi_dci0_req);
int oai_nfapi_ul_config_req(nfapi_ul_config_request_t *ul_config_req);



#endif
