/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.0  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

/*! \file PHY/defs.h
 \brief Top-level defines and structure definitions
 \author R. Knopp, F. Kaltenberger
 \date 2011
 \version 0.1
 \company Eurecom
 \email: knopp@eurecom.fr,florian.kaltenberger@eurecom.fr
 \note
 \warning
*/
#ifndef __PHY_DEFS_COMMON__H__
#define __PHY_DEFS_COMMON__H__

#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include "common_lib.h"

#include <pthread.h>

/// Context data structure for RX/TX portion of subframe processing
typedef struct {
  /// Component Carrier index
  uint8_t              CC_id;
  /// timestamp transmitted to HW
  openair0_timestamp timestamp_tx;
  /// subframe to act upon for transmission
  int subframe_tx;
  /// subframe to act upon for reception
  int subframe_rx;
  /// frame to act upon for transmission
  int frame_tx;
  /// frame to act upon for reception
  int frame_rx;

  uint16_t     HFN;
  /// \brief Instance count for RXn-TXnp4 processing thread.
  /// \internal This variable is protected by \ref mutex_rxtx.
  int instance_cnt_rxtx;
  /// pthread structure for RXn-TXnp4 processing thread
  pthread_t pthread_rxtx;
  /// pthread attributes for RXn-TXnp4 processing thread
  pthread_attr_t attr_rxtx;
  /// condition variable for tx processing thread
  pthread_cond_t cond_rxtx;
  /// mutex for RXn-TXnp4 processing thread
  pthread_mutex_t mutex_rxtx;
  /// scheduling parameters for RXn-TXnp4 thread
  struct sched_param sched_param_rxtx;

  uint32_t ra_rnti_computed;

  uint8_t dci_to_transmit;

  uint32_t frame_dscr_msg3;
  uint32_t subframe_dscr_msg3;

  uint32_t frame_msg5;
  uint32_t subframe_msg5;

  uint8_t rar_to_transmit;
  uint8_t     subframe_SP;
  uint8_t     subframe_SP2;
  int next_frame_tx;
  int next_subframe_tx;
  uint8_t SP;
  uint8_t SP2;
  uint8_t     there_is_sib23;
  int next_frame_tx_DCI;
  int next_subframe_tx_DCI;
  int subframe_delay;
  int subframe_real;
  uint32_t sheduling_info_rar;
  uint8_t flag_scrambling;

  uint8_t flag_msg3;
  uint8_t counter_msg3;
  uint32_t frame_msg3;

  uint8_t flag_DCI_msg4;
  uint8_t flag_msg4;
  uint8_t counter_DCI_msg4;
  uint8_t counter_msg4;
  uint32_t frame_DCI_msg4;
  uint32_t frame_msg4;
  uint32_t subframe_DCI_msg4;
  uint32_t subframe_msg4;

   uint8_t  guard;
 uint8_t counter_msg5;
  uint8_t remaining_dci;

  uint8_t remaining_rar;

  uint8_t flag_msg5;

  uint32_t frame_dscr_msg5;
  uint32_t subframe_dscr_msg5;
  /// pthread structure for RXn-TXnp4 processing thread
  pthread_t pthread;
  /// pthread attributes for RXn-TXnp4 processing thread
  pthread_attr_t attr;
  /// condition variable for tx processing thread
  pthread_cond_t cond;
  /// mutex for RXn-TXnp4 processing thread
  pthread_mutex_t mutex;
  /// condition variable for tx processing thread
  pthread_cond_t cond_RUs;
  /// mutex for RXn-TXnp4 processing thread
  pthread_mutex_t mutex_RUs;
  /// \internal This variable is protected by \ref mutex_rxtx.
  int instance_cnt;
} eNB_rxtx_proc_t;

/// Context data structure for eNB subframe processing
typedef struct eNB_proc_t_s {
  /// Component Carrier index
  uint8_t              CC_id;
  /// thread index
  int thread_index;
  /// timestamp received from HW
  openair0_timestamp timestamp_rx;
  /// timestamp to send to "slave rru"
  openair0_timestamp timestamp_tx;
  /// subframe to act upon for reception
  int subframe_rx;
  /// symbol mask for IF4p5 reception per subframe
  uint32_t symbol_mask[10];
  /// subframe to act upon for PRACH
  int subframe_prach;
  /// frame to act upon for reception
  int frame_rx;
  /// frame to act upon for transmission
  int frame_tx;
  /// frame offset for secondary eNBs (to correct for frame asynchronism at startup)
  int frame_offset;
  /// frame to act upon for PRACH
  int frame_prach;
  /// \internal This variable is protected by \ref mutex_fep.
  int instance_cnt_fep;
  /// \internal This variable is protected by \ref mutex_td.
  int instance_cnt_td;
  /// \internal This variable is protected by \ref mutex_te.
  int instance_cnt_te;
  /// \brief Instance count for FH processing thread.
  /// \internal This variable is protected by \ref mutex_FH.
  int instance_cnt_FH;
  /// \brief Instance count for rx processing thread.
  /// \internal This variable is protected by \ref mutex_prach.
  int instance_cnt_prach;
  // instance count for over-the-air eNB synchronization
  int instance_cnt_synch;
  /// \internal This variable is protected by \ref mutex_asynch_rxtx.
  int instance_cnt_asynch_rxtx;
  /// pthread structure for FH processing thread
  pthread_t pthread_FH;
  /// pthread structure for eNB single processing thread
  pthread_t pthread_single;
  /// pthread structure for asychronous RX/TX processing thread
  pthread_t pthread_asynch_rxtx;
  /// flag to indicate first RX acquisition
  int first_rx;
  /// flag to indicate first TX transmission
  int first_tx;
  /// pthread attributes for parallel fep thread
  pthread_attr_t attr_fep;
  /// pthread attributes for parallel turbo-decoder thread
  pthread_attr_t attr_td;
  /// pthread attributes for parallel turbo-encoder thread
  pthread_attr_t attr_te;
  /// pthread attributes for FH processing thread
  pthread_attr_t attr_FH;
  /// pthread attributes for single eNB processing thread
  pthread_attr_t attr_single;
  /// pthread attributes for prach processing thread
  pthread_attr_t attr_prach;
  /// pthread attributes for over-the-air synch thread
  pthread_attr_t attr_synch;
  /// pthread attributes for asynchronous RX thread
  pthread_attr_t attr_asynch_rxtx;
  /// scheduling parameters for parallel fep thread
  struct sched_param sched_param_fep;
  /// scheduling parameters for parallel turbo-decoder thread
  struct sched_param sched_param_td;
  /// scheduling parameters for parallel turbo-encoder thread
  struct sched_param sched_param_te;
  /// scheduling parameters for FH thread
  struct sched_param sched_param_FH;
  /// scheduling parameters for single eNB thread
  struct sched_param sched_param_single;
  /// scheduling parameters for prach thread
  struct sched_param sched_param_prach;
  /// scheduling parameters for over-the-air synchronization thread
  struct sched_param sched_param_synch;
  /// scheduling parameters for asynch_rxtx thread
  struct sched_param sched_param_asynch_rxtx;
  /// pthread structure for parallel fep thread
  pthread_t pthread_fep;
  /// pthread structure for parallel turbo-decoder thread
  pthread_t pthread_td;
  /// pthread structure for parallel turbo-encoder thread
  pthread_t pthread_te;
  /// pthread structure for PRACH thread
  pthread_t pthread_prach;
  /// pthread structure for eNB synch thread
  pthread_t pthread_synch;
  /// condition variable for parallel fep thread
  pthread_cond_t cond_fep;
  /// condition variable for parallel turbo-decoder thread
  pthread_cond_t cond_td;
  /// condition variable for parallel turbo-encoder thread
  pthread_cond_t cond_te;
  /// condition variable for FH thread
  pthread_cond_t cond_FH;
  /// condition variable for PRACH processing thread;
  pthread_cond_t cond_prach;
  // condition variable for over-the-air eNB synchronization
  pthread_cond_t cond_synch;
  /// condition variable for asynch RX/TX thread
  pthread_cond_t cond_asynch_rxtx;
  /// mutex for parallel fep thread
  pthread_mutex_t mutex_fep;
  /// mutex for parallel turbo-decoder thread
  pthread_mutex_t mutex_td;
  /// mutex for parallel turbo-encoder thread
  pthread_mutex_t mutex_te;
  /// mutex for FH
  pthread_mutex_t mutex_FH;
  /// mutex for PRACH thread
  pthread_mutex_t mutex_prach;
  // mutex for over-the-air eNB synchronization
  pthread_mutex_t mutex_synch;
  /// mutex for asynch RX/TX thread
  pthread_mutex_t mutex_asynch_rxtx;
  /// parameters for turbo-decoding worker thread
  //  td_params tdp;
  /// parameters for turbo-encoding worker thread
  //  te_params tep;
  /// set of scheduling variables RXn-TXnp4 threads
  eNB_rxtx_proc_t proc_rxtx[2];
  /// number of slave threads
  int                  num_slaves;
  /// array of pointers to slaves
  struct eNB_proc_t_s           **slave_proc;
} eNB_proc_t;




#endif
